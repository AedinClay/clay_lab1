from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('home_app.urls')),
    path('profile/', include('profile_app.urls')),
    path('key/', include('key_app.urls')),
    path('this_week/', include('week_app.urls')),
    path('today/', include('today_app.urls'))
]
