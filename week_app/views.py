import datetime

from django.shortcuts import render

def show_week(request):
    today = datetime.datetime.today().weekday()
    return render(request, 'week.html', 
                 {"week_start_date" : get_week_start_date(today),
                  "week_end_date"   : get_week_end_date(today)   })

def get_week_start_date(today):
    start_date = datetime.datetime.today() - datetime.timedelta(days = today)
    return start_date.strftime("%m.%d.%a").upper()

def get_week_end_date(today):
    end_date = datetime.datetime.today() + datetime.timedelta(days = 6 - today)
    return end_date.strftime("%m.%d.%a").upper()
