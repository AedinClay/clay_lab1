from django.apps import AppConfig


class WeekAppConfig(AppConfig):
    name = 'week_app'
