import datetime

from django.shortcuts import render

def show_today(request):
    return render(request, 'today.html', 
                 {"date_today" : get_date_today()})

def get_date_today():
    today = datetime.datetime.today()
    return today.strftime("%m.%d.%a").upper()
