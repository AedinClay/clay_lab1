from django.apps import AppConfig


class KeyAppConfig(AppConfig):
    name = 'key_app'
