from django.shortcuts import redirect, render
from django.http import HttpResponse
from .forms import AskName

#INSERT COMMENT HERE
def ask_name(request):
    if request.method == "POST":
        form = AskName(request.POST)
        if form.is_valid():
            return render(request, 'home.html', {'name' : form.cleaned_data['text']})
    elif request.method == "GET":
        form = AskName()
        return render(request, 'home.html', {'form' : form})

def redirect_to_home(request):
    response = redirect('home/')
    return response

